HttpMocker
==========

A PHP library used to create HTTP Mock. Useful for APIs.

## Install

```
composer require drosalys-web/http-mocker
```

## Documentations

TODO

### Example

```
php -S localhost:8000 -t example/public example/public/index.php
```

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
