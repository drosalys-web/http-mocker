<?php

/*
 * This file is part of the http-mocker package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\HttpMocker\Cache;

use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResponseCacheManager
 *
 * @author Benjamin Georgeault
 */
final class ResponseCacheManager
{
    private ?AdapterInterface $cache;

    /**
     * @var string[]
     */
    private array $keys;

    /**
     * CacheManager constructor.
     * @param AdapterInterface|null $cache
     */
    public function __construct(?AdapterInterface $cache = null)
    {
        $this->cache = $cache;
        $this->keys = [];
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return null !== $this->cache;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function has(Request $request): bool
    {
        $this->throwOnDisabled();
        $key = $this->generateKey($request);

        try {
            return $this->cache->hasItem($key);
        } catch (InvalidArgumentException $e) {}

        return false;
    }

    /**
     * @param Request $request
     * @return Response|null
     */
    public function get(Request $request): ?Response
    {
        $this->throwOnDisabled();
        $key = $this->generateKey($request);

        try {
            if ($this->cache->hasItem($key)) {
                return $this->cache->getItem($key)->get();
            }
        } catch (InvalidArgumentException $e) {}

        return null;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return bool
     */
    public function set(Request $request, Response $response): bool
    {
        $this->throwOnDisabled();
        $key = $this->generateKey($request);

        try {
            $item = $this->cache->getItem($key);
            $item->set($response);
            return $this->cache->save($item) && $this->cache->commit();
        } catch (InvalidArgumentException $e) {
        }

        return false;
    }

    private function generateKey(Request $request): string
    {
        if (false === $key = array_search($request, $this->keys)) {
            $key = sha1(json_encode([
                'method' => $request->getMethod(),
                'uri' => $request->getUri(),
                'body' => (string) $request->getContent(),
                '$_POST' => $request->request->all(),
                '$_FILES' => $request->files->keys(),
            ]));

            $this->keys[$key] = $request;
        }

        return $key;
    }

    private function throwOnDisabled(): void
    {
        if (!$this->isEnabled()) {
            throw new \LogicException('Cache disabled.');
        }
    }
}
