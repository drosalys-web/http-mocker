<?php

/*
 * This file is part of the http-mocker package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\HttpMocker\Http;

/**
 * Class HeadersCleaner
 *
 * @author Benjamin Georgeault
 */
final class HeadersCleaner
{
    public function clean(array $headers): array
    {
        if (isset($headers['content-encoding'])) {
            unset($headers['content-encoding']);
        }

        if (isset($headers['vary'])) {
            unset($headers['vary']);
        }

        if (isset($headers['transfer-encoding'])) {
            unset($headers['transfer-encoding']);
        }

        if (!isset($headers['x-powered-by'])) {
            $headers['x-powered-by'] = [];
        }
        $headers['x-powered-by'][] = 'http-mocker';
        $headers['x-http-mocker'] = true;

        return $headers;
    }
}
