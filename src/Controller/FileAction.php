<?php

/*
 * This file is part of the drosalys-web/http-mocker package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\HttpMocker\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\MimeTypes;

/**
 * Class FileAction
 *
 * @author Benjamin Georgeault
 */
final class FileAction
{
    /**
     * @param Request $request
     * @param string $file
     * @return Response
     */
    public function __invoke(Request $request, string $file): Response
    {
        return new Response(file_get_contents($file), 200, [
            'Content-Type' => (new MimeTypes())->guessMimeType($file) ?? 'text/html',
        ]);
    }
}
