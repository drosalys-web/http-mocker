<?php

/*
 * This file is part of the drosalys-web/http-mocker package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\HttpMocker;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\DependencyInjection\Loader\ClosureLoader;
use Symfony\Component\DependencyInjection\Loader\DirectoryLoader;
use Symfony\Component\DependencyInjection\Loader\GlobFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Class HttpMocker
 *
 * @author Benjamin Georgeault
 */
final class HttpMocker
{
    private string $configDirectory;

    private ?string $cacheDirectory;

    private bool $debug;

    private ContainerInterface $container;

    /**
     * HttpMocker constructor.
     * @param string $configDirectory
     * @param string|null $cacheDirectory
     * @param bool $debug
     */
    public function __construct(string $configDirectory, ?string $cacheDirectory = null, bool $debug = true)
    {
        $this->configDirectory = $configDirectory;
        $this->cacheDirectory = $cacheDirectory;
        $this->debug = $debug;

        if ($debug && class_exists('Symfony\\Component\\ErrorHandler\\Debug')) {
            Debug::enable();
        }

        if (null === $cacheDirectory = $this->getCacheDirectory()) {
            $this->container = $this->buildContainer();
        } else {
            $file = $cacheDirectory.'/container.php';
            $cache = new ConfigCache($file, $this->debug);

            if (!$cache->isFresh()) {
                $container = $this->buildContainer();
                $dumper = new PhpDumper($container);
                $content = $dumper->dump([
                    'class' => 'HttpMockerServiceContainer',
                    'file' => $cache->getPath(),
                    'debug' => $this->debug,
                ]);

                $cache->write($content, $container->getResources());
            }

            require $cache->getPath();
            $this->container = new \HttpMockerServiceContainer();
            $this->container->set('container', $this->container);
        }
    }

    /**
     * Run Mocker.
     */
    public function __invoke()
    {
        /** @var HttpKernel $kernel */
        $kernel = $this->container->get(HttpKernel::class);

        $response = $kernel->handle($request = Request::createFromGlobals());
        $response->send();

        $kernel->terminate($request, $response);
    }

    /**
     * @return string
     */
    private function getMockerDirectory(): string
    {
        return realpath(__DIR__.'/..');
    }

    /**
     * @return string|null
     */
    private function getCacheDirectory(): ?string
    {
        if (null !== $this->cacheDirectory) {
            return $this->cacheDirectory.'/http_mocker';
        }

        return null;
    }

    /**
     * @return ContainerBuilder
     */
    private function buildContainer(): ContainerBuilder
    {
        $container = new ContainerBuilder();
        $container->addObjectResource($this);

        $this->initPass($container);
        $this->initParameters($container);
        $this->initServices($container);

        $container->compile();

        return $container;
    }

    /**
     * @param ContainerBuilder $container
     */
    private function initPass(ContainerBuilder $container)
    {
        $container->addCompilerPass(new RegisterListenersPass());
        $container->registerForAutoconfiguration(EventSubscriberInterface::class)
            ->addTag('kernel.event_subscriber')
        ;
    }

    /**
     * @param ContainerBuilder $container
     */
    private function initParameters(ContainerBuilder $container)
    {
        $params = [
            'debug' => $this->debug,
            'mocker.mocker_directory' => $this->getMockerDirectory(),
            'mocker.cache_directory' => $this->getCacheDirectory(),
            'mocker.config_directory' => $this->configDirectory,
        ];

        foreach ($params as $param => $value) {
            $container->setParameter($param, $value);
        }
    }

    /**
     * @param ContainerBuilder $container
     */
    private function initServices(ContainerBuilder $container)
    {
        $locator = new FileLocator($this);
        $resolver = new LoaderResolver([
            new YamlFileLoader($container, $locator),
            new GlobFileLoader($container, $locator),
            new DirectoryLoader($container, $locator),
            new ClosureLoader($container),
        ]);

        $loader = new DelegatingLoader($resolver);

        $loader->load(function (ContainerBuilder $container) use ($loader) {
            $container->setAlias(get_class($this), 'kernel')
                ->setPublic(true)
            ;
            $container->addObjectResource($this);

            $dirs = [
                $this->getMockerDirectory().'/config',
                $this->configDirectory,
            ];

            $fs = new Filesystem();
            foreach ($dirs as $dir) {
                if ($fs->exists($dir)) {
                    $confDir = $dir;

                    $loader->load($confDir.'/{services}/*.{yaml,yml}', 'glob');
                    $loader->load($confDir.'/{services}.{yaml,yml}', 'glob');
                }
            }
        });

        if (null !== $cacheDirectory = $this->getCacheDirectory()) {
            $container->setDefinition(AdapterInterface::class, new Definition(FilesystemAdapter::class, [
                'remote_response',
                0,
                $cacheDirectory
            ]));
        }
    }
}
